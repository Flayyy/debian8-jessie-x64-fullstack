#!/bin/bash
cd /vagrant

# I. Add sources
# ==============
# DotDeb source
cat >/etc/apt/sources.list.d/dotdeb.list <<EOL
deb http://packages.dotdeb.org jessie all
deb-src http://packages.dotdeb.org jessie all
EOL
wget https://www.dotdeb.org/dotdeb.gpg
sudo apt-key add dotdeb.gpg
rm dotdeb.gpg

# nginx source
cat >/etc/apt/sources.list.d/nginx.list <<EOL
deb http://nginx.org/packages/mainline/debian/ jessie nginx
deb-src http://nginx.org/packages/mainline/debian/ jessie nginx
EOL
wget http://nginx.org/keys/nginx_signing.key
sudo apt-key add nginx_signing.key
rm nginx_signing.key

# mysql source
wget http://dev.mysql.com/get/mysql-apt-config_0.8.0-1_all.deb
dpkg -i mysql-apt-config_0.8.0-1_all.deb
rm mysql-apt-config_0.8.0-1_all.deb

# nodejs source
curl -sL https://deb.nodesource.com/setup_6.x | -E bash -


# II. Install and configure packages
# ==================================
apt-get update
apt-get -y upgrade

# II.A - Tools
apt-get install -y curl zip unzip git putty-tools

# II.B - nginx
apt-get install -y nginx
sed -i -- 's/user  nginx/user  www-data/g' /etc/nginx/nginx.conf
service nginx restart

# II.C - php7.0
apt-get install -y php7.0 php7.0-fpm php7.0-gd php7.0-mysql php7.0-cli php7.0-common php7.0-curl php7.0-opcache php7.0-json php7.0-imap php7.0-intl php7.0-mbstring php7.0-dom php7.0-zip

# II.D - MySQL server
apt-get install -y mysql-server
mysql_secure_installation

# II.E - Composer 
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# II.F - nodejs
apt-get install -y nodejs npm
npm install -g bower

# Generate ppk keys
puttygen /vagrant/.vagrant/machines/default/virtualbox/private_key -o /vagrant/.vagrant/machines/default/virtualbox/private_key.ppk