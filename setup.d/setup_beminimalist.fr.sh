#!/bin/bash

# Clone repo
ssh-keyscan 'bitbucket.org' | sudo tee -a ~/.ssh/known_hosts
eval `ssh-agent -s`
cp /vagrant/.ssh/bitbucket_rsa ~/.ssh/bitbucket_rsa
chmod 600 ~/.ssh/bitbucket_rsa
ssh-add ~/.ssh/bitbucket_rsa
git clone git@bitbucket.org:Flayyy/beminimalist.fr.git /var/www/beminimalist.fr

# Import vhost to nginx conf
mv beminimalist.fr.conf /etc/nginx/conf.d/beminimalist.fr
service nginx reload

# Setup things
cd /var/www/beminimalist.fr
composer install
npm install
bower install